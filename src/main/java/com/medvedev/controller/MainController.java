package com.medvedev.controller;

import com.medvedev.SplashWindow;
import com.medvedev.service.DirectoryService;
import com.medvedev.service.SystemService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {
    private DirectoryService directoryService;
    private SystemService systemService;
    private Stage stage;

    @FXML
    public AnchorPane root;
    @FXML
    private VBox workspace;
    @FXML
    private ScrollPane scrollPane;
    @FXML
    private ImageView backButton;
    private ContextMenuController contextMenu;


    public MainController(Stage stage) {
        load(stage);
    }

    public static void playErrorSound() {
        Media pick = new Media(MainController.class.getResource("/sound/error").toString());
        MediaPlayer player = new MediaPlayer(pick);
        player.play();
    }


    private void load(Stage stage) {
        try {
            this.stage = stage;
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/main_view.fxml"));
            loader.setController(this);
            Scene scene = new Scene(loader.load());
            stage.setScene(scene);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void showContextMenu(ContextMenuEvent event) {
        contextMenu.show(event.getScreenX(), event.getScreenY());
    }

    private void createFolder(ActionEvent actionEvent) {
        DiskElement diskElement = createDiskElement(directoryService.createNewDirectory());
        diskElement.makeTitleEditable();
    }

    public void refresh() {
        workspace.getChildren().clear();
        for (File file : directoryService.getAllFiles()) {
            createDiskElement(file);
        }
    }

    private DiskElement createDiskElement(File file) {
        DiskElement diskElement = null;
        diskElement = new DiskElement(file);
        diskElement.setOnChange(h -> refresh());
        diskElement.setOnOpen(this::goToNextDirectory);
        diskElement.setOnReplace(this::replace);
        diskElement.setRenameAction((source, dist) -> {
            if (source.renameTo(dist)) {
                return dist;
            }
            playErrorSound();
            return source;
        });
        diskElement.setOnDelete(f -> {
            try {
                directoryService.delete(f);
            } catch (IOException e) {
                playErrorSound();
            }
        });
        workspace.getChildren().add(diskElement.getRoot());
        return diskElement;
    }

    private void replace(File file) {
        try {
            directoryService.replaceToCurrent(file);
            contextMenu.getPasteItem().setVisible(false);
            refresh();
        } catch (IOException e) {
            playErrorSound();
        }
    }

    private void goToNextDirectory(File f) {
        directoryService.next(f);
        refresh();
    }

    @FXML
    public void goPrevDir() {
        if (directoryService.back()) {
            refresh();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        directoryService = new DirectoryService();
        systemService = new SystemService();
        contextMenu = ContextMenuController.getNewInstance(stage);
        contextMenu.getCreateFolderItem().setOnAction(this::createFolder);
        refresh();
    }

    @FXML
    public void showInputView() {
        new InputController().setOnOkClick(title -> {
            systemService.saveProtocolRunningPrograms(title);
            refresh();
        });
    }

    @FXML
    public void runInfoViewProcess() {
        systemService.saveProcessInfoLog();
        systemService.runInNewProcess(SplashWindow.class, null);
    }
}
