package com.medvedev.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.util.Objects;
import java.util.function.Consumer;

public class InputController {
    @FXML
    public Text titleField;
    @FXML
    public Button okButton;
    @FXML
    public Button cancelButton;
    @FXML
    private TextField inputField;
    private Consumer<String> onOkClick;
    private Stage stage;


    public InputController() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/input_view.fxml"));
            loader.setController(this);
            stage = new Stage(StageStyle.TRANSPARENT);
            stage.setScene(new Scene(loader.load()));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @FXML
    public void ok() {
        String title = inputField.getText();
        if (!title.isEmpty()) {
            if (Objects.nonNull(onOkClick)) {
                onOkClick.accept(title);
                hide();
            }
        } else {
            MainController.playErrorSound();
        }
    }

    public void setOnOkClick(Consumer<String> onOkClick) {
        this.onOkClick = onOkClick;
    }

    @FXML
    public void hide() {
        stage.close();
    }
}
