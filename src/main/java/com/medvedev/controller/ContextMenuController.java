package com.medvedev.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

import java.io.IOException;

public class ContextMenuController {
    private static ContextMenuController instance;
    private ContextMenu contextMenu;
    @FXML
    private MenuItem remove;
    @FXML
    private MenuItem rename;
    @FXML
    private MenuItem createFolder;
    @FXML
    private MenuItem replace;
    @FXML
    private MenuItem paste;
    private Stage stage;


    private ContextMenuController(Stage stage) {
        load();
        this.stage = stage;
    }


    private void load() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/ui_components/context_menu.fxml"));
            loader.setController(this);
            this.contextMenu = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void show(double x, double y) {
        contextMenu.show(stage, x, y);
    }

    public void high() {
        contextMenu.hide();
    }

    public MenuItem getRemoveItem() {
        return remove;
    }

    public MenuItem getRenameItem() {
        return rename;
    }

    public static ContextMenuController getNewInstance(Stage stage) {
        if (instance == null) {
            instance = new ContextMenuController(stage);
        }
        return instance;
    }

    public static ContextMenuController getInstance() {
        return instance;
    }

    public ContextMenu getRoot() {
        return contextMenu;
    }

    public MenuItem getCreateFolderItem() {
        return createFolder;
    }

    public MenuItem getReplaceItem() {
        return replace;
    }

    public MenuItem getPasteItem() {
        return paste;
    }
}
