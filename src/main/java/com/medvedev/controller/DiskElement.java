package com.medvedev.controller;

import com.medvedev.util.FileUtil;
import com.medvedev.util.ImageUtil;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;

public class DiskElement {
    private File file;
    @FXML
    private GridPane root;
    @FXML
    private ImageView icon;
    @FXML
    private Text title;
    @FXML
    private Text lastModified;
    @FXML
    private Text totalSpace;
    @FXML
    private TextField renameField;
    private ContextMenuController contextMenu;
    private EventHandler<Event> onChange;
    private Consumer<File> onOpen;
    private Consumer<File> onReplace;
    private BinaryOperator<File> rename;
    private Consumer<File> onDelete;
    private boolean light;


    public DiskElement(File file) {
        load(file);
        contextMenu = ContextMenuController.getInstance();
    }


    private void load(File file) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass()
                    .getResource("/ui_components/disk_element.fxml"));
            loader.setController(this);
            loader.load();
            this.setFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void focus() {
        if (!light) {
            root.setStyle("-fx-border-color: #79B6D6");
        }
    }

    @FXML
    public void unFocus() {
        if (!light) {
            root.setStyle("-fx-border-color: #1E1E1E");
        }
    }

    @FXML
    public void open(MouseEvent event) {
        if (event.getClickCount() == 2) {
            openFile(file);
        }
    }

    @FXML
    public void showContextMenu(ContextMenuEvent event) {
        contextMenu.getRoot().setOnHiding(a -> contextMenu.getRoot().getItems().forEach(ite -> setVisibleIfDirDestination(ite, false)));
        contextMenu.getRoot().getItems().forEach(item -> setVisibleIfDirDestination(item, true));
        contextMenu.getRemoveItem().setOnAction(l -> deleteFile(file, l));
        contextMenu.getRenameItem().setOnAction(n -> makeTitleEditable());
        contextMenu.getReplaceItem().setOnAction(r -> replace());
        contextMenu.show(event.getScreenX(), event.getScreenY());
    }

    private void setVisibleIfDirDestination(MenuItem item, boolean visible) {
        for (String styleClassName : item.getStyleClass()) {
            if (styleClassName.equals("dir")) {
                item.setVisible(visible);
            }
        }
    }

    private void replace() {
        light();
        contextMenu.getPasteItem().setVisible(true);
        if (Objects.nonNull(onReplace)) {
            contextMenu.getPasteItem().setOnAction(h -> onReplace.accept(file));
        }
    }

    private void light() {
        light = true;
        root.setStyle("-fx-background-color: #79B6D6; -fx-opacity: 0.9");
    }

    @FXML
    private void rename(KeyEvent event) {
        if (event.getCode().equals(KeyCode.ENTER)) {
            String freshName = renameField.getText();
            if (!freshName.isEmpty()) {
                if (Objects.nonNull(rename)) {
                    file = rename.apply(file, getNewFile(freshName));
                    title.setText(file.getName());
                    renameField.setVisible(false);
                    title.setVisible(true);
                    executeChangeAction(event);
                }
            } else {
                MainController.playErrorSound();
            }
        }
    }

    private File getNewFile(String newTitle) {
        File parent = file.getParentFile();
        if (parent != null) {
            return new File(parent.getPath() + '/' + newTitle);
        }
        return file;
    }

    private void openFile(File file) {
        BasicThreadFactory factory = new BasicThreadFactory.Builder().build();
        ExecutorService executorService = Executors.newSingleThreadExecutor(factory);
        if (Desktop.isDesktopSupported() && file.isFile() && onOpen != null) {
            executorService.execute(() -> {
                try {
                    Desktop.getDesktop().open(file);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } else {
            onOpen.accept(file);
        }
    }

    private void deleteFile(File file, ActionEvent l) {
        if (Objects.nonNull(onDelete)) {
            onDelete.accept(file);
            executeChangeAction(l);
        }
    }

    private void executeChangeAction(Event event) {
        if (Objects.nonNull(onChange)) {
            onChange.handle(event);
        }
    }

    public void makeTitleEditable() {
        title.setVisible(false);
        renameField.setText(title.getText());
        renameField.setVisible(true);
    }

    private void setTotalSpace(long totalSpace) {
        if (totalSpace > 1024 && totalSpace <= 1048576) {
            this.totalSpace.setText(totalSpace / 1024 + " KB");
        } else if (totalSpace > 1048576 && totalSpace <= 1073741824) {
            this.totalSpace.setText(totalSpace / 1048576 + " MB");
        } else if (totalSpace > 1073741824) {
            this.totalSpace.setText(totalSpace / 1073741824 + " GB");
        } else {
            this.totalSpace.setText(totalSpace + " B");
        }
    }

    private void setIcon(File file) {
        if (file.isDirectory()) {
            icon.setImage(ImageUtil.getFolder());
        } else {
            icon.setImage(ImageUtil.getFile());
        }
    }

    public File getFile() {
        return file;
    }

    public Pane getRoot() {
        return root;
    }

    public void setFile(File file) {
        title.setText(file.getName());
        lastModified.setText(new Date(file.lastModified()).toString());
        setTotalSpace(FileUtil.getSize(file));
        setIcon(file);
        this.file = file;
    }

    public void setOnChange(EventHandler<Event> value) {
        this.onChange = value;
    }

    public void setOnOpen(Consumer<File> value) {
        this.onOpen = value;
    }

    public void setOnReplace(Consumer<File> value) {
        onReplace = value;
    }

    public void setRenameAction(BinaryOperator<File> value) {
        this.rename = value;
    }

    public void setOnDelete(Consumer<File> onDelete) {
        this.onDelete = onDelete;
    }
}
