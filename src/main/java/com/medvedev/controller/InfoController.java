package com.medvedev.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ResourceBundle;

public class InfoController implements Initializable {
    @FXML
    public Text infoField;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            FileInputStream inputStream = new FileInputStream(new File("root/system/Process_Info"));
            byte[] bytes = inputStream.readAllBytes();
            infoField.setText(new String(bytes, StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
