package com.medvedev.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class SystemService {
    private final String SYSTEM_DIR_PATH = "root/system/";
    private final String DEFAULT_TITLE_INFO_LOG = "Process_Info";


    public void saveProtocolRunningPrograms(String fileTitle) {
        StringBuilder info = new StringBuilder("Протокол запущенных процессов\n\n"
                + String.format("%8s %33s %30s", "PID", "USER", "START_INSTANT \n"));
        ProcessHandle.allProcesses().forEach(process ->
                info.append(processDetails(process) + '\n'));
        out(info.toString(), fileTitle);
    }

    private static String processDetails(ProcessHandle process) {
        return String.format("%8d | %30s | %26s |",
                process.pid(),
                text(process.info().user()),
                text(process.info().startInstant()));
    }

    private static String text(Optional<?> optional) {
        return optional.map(Object::toString).orElse("-");
    }

    private void out(String info, String fileTitle) {
        try (FileOutputStream stream = new FileOutputStream(new File(SYSTEM_DIR_PATH + fileTitle))) {
            stream.write(info.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveProcessInfoLog() {
        String info = String.format(
                "Пользователь процесса: %s\nКоличество потоков процесса: %d"
                , System.getProperty("user.name"), Thread.activeCount());
        out(info, DEFAULT_TITLE_INFO_LOG);
    }

    public void runInNewProcess(Class klass, List<String> args) {
        try {
            String javaHome = System.getProperty("java.home");
            String javaBin = javaHome +
                    File.separator + "bin" +
                    File.separator + "java";
            String classpath = System.getProperty("java.class.path");
            String className = klass.getName();

            List<String> command = new LinkedList();
            command.add(javaBin);
            command.add("-cp");
            command.add(classpath);
            command.add(className);
            if (args != null) {
                command.addAll(args);
            }

            ProcessBuilder builder = new ProcessBuilder(command);

            builder.inheritIO().start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
