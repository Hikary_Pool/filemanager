package com.medvedev.service;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;

public class DirectoryService {
    private final String HOME_DIRECTORY = "root";
    private File currentDirectory;


    public DirectoryService() {
        currentDirectory = new File(HOME_DIRECTORY);
    }


    public File[] getAllFiles() {
        return currentDirectory.listFiles();
    }

    public void next(File dir) {
        currentDirectory = dir;
    }

    public boolean back() {
        String parentPath = currentDirectory.getParent();
        if (Objects.nonNull(parentPath)) {
            currentDirectory = new File(parentPath);
            return true;
        }
        return false;
    }

    public File getCurrentDirectory() {
        return currentDirectory;
    }

    public File createNewDirectory() {
        File file = new File(currentDirectory.getPath() + "/" + createUniqTitle());
        file.mkdir();
        return file;
    }

    public String createUniqTitle() {
        final String title = currentDirectory.getName();
        String postfix = "";
        int index = 0;

        File[] files = getAllFiles();
        boolean titleIsExists = true;
        while (titleIsExists) {
            String finalPostfix = postfix;
            titleIsExists = Arrays.stream(files).anyMatch(file -> file.getName().equals(title + finalPostfix));
            if (titleIsExists) {
                postfix = String.valueOf(index++);
            }
        }
        return title + postfix;
    }

    public void replaceToCurrent(File source) throws IOException {
        move(source, currentDirectory);
    }

    private File getRealFileDest(File dist, String name) {
        return new File(dist.getPath() + '/' + name);
    }

    public File move(File source, File dist) throws IOException {
        if (source.isDirectory()) {
            FileUtils.moveDirectory(source, getRealFileDest(dist, source.getName()));
        } else {
            FileUtils.moveFile(source, getRealFileDest(dist, source.getName()));
        }
        return dist;
    }

    public void delete(File file) throws IOException {
        if (file.isFile()) {
            file.delete();
        } else {
            FileUtils.deleteDirectory(file);
        }
    }
}
