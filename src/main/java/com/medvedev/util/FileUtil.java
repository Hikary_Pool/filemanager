package com.medvedev.util;

import java.io.File;
import java.util.Objects;

public class FileUtil {
    public static long getSize(File dir) {
        long length = 0;
        File[] dirs = dir.listFiles();
        if (Objects.nonNull(dirs)) {
            for (File file : dirs) {
                if (file.isFile())
                    length += file.length();
                else
                    length += getSize(file);
            }
        } else {
            length = dir.length();
        }
        return length;
    }
}
