package com.medvedev.util;

import javafx.scene.image.Image;

public class ImageUtil {
    private static Image folder;
    private static Image file;


    private ImageUtil() {
    }


    public static Image getFolder() {
        if (folder == null) {
            folder = new Image("/icon/folder.png");
        }
        return folder;
    }

    public static Image getFile() {
        if (file == null) {
            file = new Image("/icon/file.png");
        }
        return file;
    }
}
