package com.medvedev;


import com.medvedev.controller.MainController;
import com.medvedev.service.SystemService;
import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.File;


public class Main extends Application {

    public static void main(String[] args) {
        prepareDirectoryStructure();
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        new MainController(primaryStage);
        primaryStage.getIcons().add(new Image("/icon/ico.png"));
        primaryStage.show();
    }

    private static void prepareDirectoryStructure() {
        File dir = new File("root/system");
        if (dir.mkdirs()) {
            SystemService.setNonDeletable(dir);
        }
    }
}
